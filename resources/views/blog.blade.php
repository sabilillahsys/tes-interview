@extends('layouts.home.layout')
@section('content')
<!-- News With Sidebar Start -->
    <div class="container-fluid mt-5 pt-3">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row">
                    @foreach($post as $data)
                        <div class="col-lg-4">
                            <div class="position-relative mb-3">
                                <img class="img-fluid w-100" src="{{url('/').Storage::url($data->img)}}" style="object-fit: cover;">
                                <div class="bg-white border border-top-0 p-4">
                                    <div class="mb-2">
                                    @foreach($data->post_kategori_log as $kategori)
                                        <a class="badge badge-primary text-uppercase font-weight-semi-bold p-2 mr-2"
                                            href="{{route('kategori',$kategori->post_kategori->slug)}}">{{$kategori->post_kategori->judul}}</a>
                                    @endforeach   
                                    </div>
                                    <a class="h4 d-block mb-3 text-secondary text-uppercase font-weight-bold" href="{{route('post',$data->slug)}}">{{$data->judul}}</a>
                                </div>
                                <div class="d-flex justify-content-between bg-white border border-top-0 p-4">
                                    <div class="d-flex align-items-center">
                                        <small>{{$data->user->name}}</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    {{ $post->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- News With Sidebar End -->
    @endsection