@extends('layouts.home.layout')
@section('content')
    <!-- Breaking News Start -->
    <div class="container-fluid mt-5 mb-3 pt-3">
        <div class="container">
            <div class="row align-items-center">
            </div>
        </div>
    </div>
    <!-- Breaking News End -->
    <!-- News With Sidebar Start -->
    <div class="container-fluid">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <!-- News Detail Start -->
                    <div class="position-relative mb-3">
                        <img class="img-fluid w-100" src="{{url('/').Storage::url($post->img)}}" style="object-fit: cover;">
                        <div class="bg-white border border-top-0 p-4">
                            <div class="mb-3">
                            @foreach($post->post_kategori_log as $kategori)
                                <a class="badge badge-primary text-uppercase font-weight-semi-bold p-2 mr-2"
                                    href="{{route('kategori',$kategori->post_kategori->slug)}}">{{$kategori->post_kategori->judul}}</a>
                            @endforeach
                            </div>
                            <h1 class="mb-3 text-secondary text-uppercase font-weight-bold">{{$post->judul}}</h1>
                            {!!$post->isi!!}
                        </div>
                    </div>
                    <!-- News Detail End -->

                    <!-- Comment List Start -->
                    <div class="mb-3">
                        <div class="section-title mb-0">
                            <h4 class="m-0 text-uppercase font-weight-bold">Comments</h4>
                        </div>
                        <div class="bg-white border border-top-0 p-4">
                            @foreach($comment as $komen)
                            <div class="media mb-4">
                                <div class="media-body">
                                    <h6 class="text-secondary font-weight-bold">{{$komen->nama}}</small></h6>
                                    <p>{{$komen->comment}}</p>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    <!-- Comment List End -->

                    <!-- Comment Form Start -->
                    <div class="mb-3">
                        <div class="section-title mb-0">
                            <h4 class="m-0 text-uppercase font-weight-bold">Leave a comment</h4>
                        </div>
                        <div class="bg-white border border-top-0 p-4">
                            <form action="{{route('comment')}}" method="post">
                            @csrf
                                <div class="form-row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Nama Lengkap *</label>
                                            <input type="text" class="form-control" placeholder="Nama Lengkap" name="nama" required>
                                            <input type="hidden" class="form-control" name="post_id" value="{{$post->id}}">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Email *</label>
                                            <input type="email" class="form-control" name="email" placeholder="Email">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Pesan *</label>
                                    <textarea  cols="30" name="comment" placeholder="Pesan" rows="5" class="form-control"></textarea>
                                </div>
                                <div class="form-group mb-0">
                                    <input type="submit" value="Comment"
                                        class="btn btn-primary font-weight-semi-bold py-2 px-3">
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- Comment Form End -->
                </div>

                <div class="col-lg-4">
                    <!-- Popular News Start -->
                    <div class="mb-3">
                        <div class="section-title mb-0">
                            <h4 class="m-0 text-uppercase font-weight-bold">Tranding News</h4>
                        </div>
                        <div class="bg-white border border-top-0 p-3">
                        @foreach($post1 as $data)
                            <div class="d-flex align-items-center bg-white mb-3" style="height: 110px;">
                                <div class="w-100 h-100 px-3 d-flex flex-column justify-content-center border border-left-0">
                                    <div class="mb-2">
                                    @foreach($data->post_kategori_log as $kategori)
                                        <a class="badge badge-primary text-uppercase font-weight-semi-bold p-1 mr-2" href="{{route('kategori',$kategori->post_kategori->slug)}}">{{$kategori->post_kategori->judul}}</a>
                                    @endforeach
                                    </div>
                                    <a class="h6 m-0 text-secondary text-uppercase font-weight-bold" href="{{route('post',$data->slug)}}">{{$data->judul}}</a>
                                </div>
                            </div>
                        @endforeach
                        </div>
                    </div>
                    <!-- Popular News End -->
                    <!-- Tags Start -->
                    <div class="mb-3">
                        <div class="section-title mb-0">
                            <h4 class="m-0 text-uppercase font-weight-bold">Tags</h4>
                        </div>
                        <div class="bg-white border border-top-0 p-3">
                            <div class="d-flex flex-wrap m-n1">
                                @foreach($arr_tag as $key => $tags)
                                @php($tag=str_replace(' ','-',$tags))
                                <h5 class="btn btn-sm btn-outline-secondary m-1">{{$tags}}</h5>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <!-- Tags End -->
                </div>
            </div>
        </div>
    </div>
    <!-- News With Sidebar End -->
    @endsection