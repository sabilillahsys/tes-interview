    <!-- Navbar Start -->
    <div class="container-fluid p-0">
        <nav class="navbar navbar-expand-lg bg-dark navbar-dark py-2 py-lg-0 px-lg-5">
            <a href="index.html" class="navbar-brand d-block d-lg-none">
                <h1 class="m-0 display-4 text-uppercase text-primary">Hasun<span class="text-white font-weight-normal">News</span></h1>
            </a>
            <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbarCollapse">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse justify-content-between px-0 px-lg-3" id="navbarCollapse">
                <div class="navbar-nav mr-auto py-0">
                    <a href="{{route('homepage')}}" class="nav-item nav-link {{ (request()->routeIs('homepage')) ? 'active' : '' }}">Home</a>
                    <a href="{{route('blog')}}" class="nav-item nav-link {{ (request()->routeIs('blog')) ? 'active' : '' }}">Blog</a>
                </div>
                <div class="input-group ml-auto d-none d-lg-flex" style="width: 100%; max-width: 300px;">
                <form action="{{route('blog')}}" method="get" >
                    <input type="text" class="form-control border-0" placeholder="Keyword" name="q">
                    <button type="submit"></button>
                </form>
                </div>
            </div>
        </nav>
    </div>
    <!-- Navbar End -->