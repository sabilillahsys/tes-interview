@extends('layouts.home.layout')
@section('content')
    <!-- Main News Slider Start -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-7 px-0">
                <div class="owl-carousel main-carousel position-relative">
                @foreach($post as $data)
                    <div class="position-relative overflow-hidden" style="height: 500px;">
                        <img class="img-fluid h-100" src="{{url('/').Storage::url($data->img)}}" style="object-fit: cover;">
                        <div class="overlay">
                            <div class="mb-2">
                                @foreach($data->post_kategori_log as $kategori)
                                <a class="badge badge-primary text-uppercase font-weight-semi-bold p-2 mr-2"
                                    href="{{route('kategori',$kategori->post_kategori->slug)}}">{{$kategori->post_kategori->judul}}</a>
                                @endforeach
                            </div>
                            <a class="h2 m-0 text-white text-uppercase font-weight-bold" href="{{route('post',$data->slug)}}">{{$data->judul}}</a>
                        </div>
                    </div>
                @endforeach
                </div>
            </div>
            <div class="col-lg-5 px-0">
                <div class="row mx-0">
                @foreach($post1 as $datas)
                    <div class="col-md-6 px-0">
                        <div class="position-relative overflow-hidden" style="height: 250px;">
                            <img class="img-fluid w-100 h-100" src="{{url('/').Storage::url($datas->img)}}" style="object-fit: cover;">
                            <div class="overlay">
                                <div class="mb-2">
                                @foreach($datas->post_kategori_log as $kategori)
                                    <a class="badge badge-primary text-uppercase font-weight-semi-bold p-2 mr-2"
                                        href="{{route('kategori',$kategori->post_kategori->slug)}}">{{$kategori->post_kategori->judul}}</a>
                                @endforeach
                                </div>
                                <a class="h6 m-0 text-white text-uppercase font-weight-semi-bold" href="{{route('post',$data->slug)}}">{{$datas->judul}}</a>
                            </div>
                        </div>
                    </div>
                @endforeach
                </div>
            </div>
        </div>
    </div>
    <!-- Main News Slider End -->


    <!-- Breaking News Start -->
    <div class="container-fluid bg-dark py-3 mb-3">
        <div class="container">
            <div class="row align-items-center bg-dark">
                <div class="col-12">
                    <div class="d-flex justify-content-between">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Breaking News End -->


    <!-- Featured News Slider Start -->
    <div class="container-fluid pt-5 mb-3">
        <div class="container">
            <div class="section-title">
                <h4 class="m-0 text-uppercase font-weight-bold">Recent Post</h4>
            </div>
            <div class="owl-carousel news-carousel carousel-item-4 position-relative">
            @foreach($post2 as $datas)
                <div class="position-relative overflow-hidden" style="height: 300px;">
                    <img class="img-fluid h-100" src="{{url('/').Storage::url($datas->img)}}" style="object-fit: cover;">
                    <div class="overlay">
                        <div class="mb-2">
                            @foreach($datas->post_kategori_log as $kategori)
                            <a class="badge badge-primary text-uppercase font-weight-semi-bold p-2 mr-2"
                                href="{{route('kategori',$kategori->post_kategori->slug)}}">{{$kategori->post_kategori->judul}}</a>
                            @endforeach
                        </div>
                        <a class="h6 m-0 text-white text-uppercase font-weight-semi-bold" href="{{route('post',$data->slug)}}">{{$datas->judul}}</a>
                    </div>
                </div>
            @endforeach
            </div>
        </div>
    </div>
    <!-- Featured News Slider End -->
    @endsection