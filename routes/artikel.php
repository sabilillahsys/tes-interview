<?php
//untuk SEO
use App\Http\Controllers\ArtikelController;

Route::group(['middleware' => ['web','roles']],function(){
    Route::group(['roles'=>['Master','Penulis']],function(){
        Route::group([
            'prefix' => 'artikel','as' => 'artikel.',
        ], function(){
            Route::get('/form_artikel', [ArtikelController::class, 'form_artikel'])->name('form_artikel');
            Route::get('/form_update_post/{id}', [ArtikelController::class, 'form_update_post'])->name('form_update_post');

            Route::get('/data_artikel_all', [ArtikelController::class, 'data_artikel_all'])->name('data_artikel_all');
            Route::get('/json_artikel_all', [ArtikelController::class, 'json_artikel_all'])->name('json_artikel_all');

            Route::post('/input_post', [ArtikelController::class, 'input_post'])->name('input_post');
            Route::post('/update_post/{id}', [ArtikelController::class, 'update_post'])->name('update_post');
            Route::get('/delete_post/{id}', [ArtikelController::class, 'delete_post'])->name('delete_post');

            Route::get('/data_post_kategori', [ArtikelController::class, 'data_post_kategori'])->name('data_post_kategori');
            Route::get('/json_post_kategori', [ArtikelController::class, 'json_post_kategori'])->name('json_post_kategori');
            Route::post('/input_post_kategori', [ArtikelController::class, 'input_post_kategori'])->name('input_post_kategori');
            Route::post('/update_post_kategori/{id}', [ArtikelController::class, 'update_post_kategori'])->name('update_post_kategori');
            Route::get('/delete_post_kategori/{id}', [ArtikelController::class, 'delete_post_kategori'])->name('delete_post_kategori');
            
        });
    });
});