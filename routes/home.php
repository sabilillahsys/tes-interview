<?php
    use App\Http\Controllers\HomepageController;
    use App\Http\Controllers\LoginController;

    Route::get('/', [HomepageController::class, 'index'])->name('homepage');
    Route::get('/blog', [HomepageController::class, 'blog'])->name('blog');
    Route::get('/populer', [HomepageController::class, 'populer'])->name('populer');
    Route::get('/kategori/{slug}', [HomepageController::class, 'kategori'])->name('kategori');
    Route::get('/login', [HomepageController::class, 'login'])->name('login');
    Route::get('/notfound', [HomepageController::class, 'notfound'])->name('notfound');
    Route::get('/sitemap.xml', [HomepageController::class, 'sitemap'])->name('sitemap');

    Route::post('masuk', [LoginController::class, 'masuk'])->name('masuk');
    Route::get('keluar', [LoginController::class, 'keluar'])->name('keluar');

    Route::get('/{slug}', [HomepageController::class, 'post'])->name('post');
    Route::get('site/sitemap.xml', [HomepageController::class, 'postmap'])->name('postmap');

    Route::post('comment', [HomepageController::class, 'comment'])->name('comment');


