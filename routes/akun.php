<?php
//1
use App\Http\Controllers\AkunController;

Route::group(['middleware' => ['web','roles']],function(){
    Route::group(['roles'=>['Master']],function(){
        Route::group([
            'prefix' => 'master','as' => 'master.',
        ], function(){
            Route::get('/', [AkunController::class, 'master'])->name('master');
            Route::get('/data_master', [AkunController::class, 'data_master'])->name('data_master');
            Route::get('/json_master', [AkunController::class, 'json_master'])->name('json_master');
            Route::post('/input_master', [AkunController::class, 'input_master'])->name('input_master');
            Route::post('/update_master/{id}', [AkunController::class, 'update_master'])->name('update_master');
            Route::get('/delete_master/{id}', [AkunController::class, 'delete_master'])->name('delete_master');
        });
    });
});
Route::group(['middleware' => ['web','roles']],function(){
    Route::group(['roles'=>['Master','Penulis']],function(){
        Route::group([
            'prefix' => 'penulis','as' => 'penulis.',
        ], function(){
            Route::get('/', [AkunController::class, 'penulis'])->name('penulis');
            Route::get('/data_penulis', [AkunController::class, 'data_penulis'])->name('data_penulis');
            Route::get('/json_penulis', [AkunController::class, 'json_penulis'])->name('json_penulis');
            Route::post('/input_penulis', [AkunController::class, 'input_penulis'])->name('input_penulis');
            Route::post('/update_penulis/{id}', [AkunController::class, 'update_penulis'])->name('update_penulis');
            Route::get('/delete_penulis/{id}', [AkunController::class, 'delete_penulis'])->name('delete_penulis');
        });
    });
});