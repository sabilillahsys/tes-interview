<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\post;
use App\Models\post_kategori;
use App\Models\post_comment;
use Auth;
use DataTables;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
class HomepageController extends Controller
{
    public function index()
    {
        $post=post::with(['post_kategori_log'])->inRandomOrder()->paginate(3);
        $post1=post::with(['post_kategori_log'])->inRandomOrder()->paginate(4);
        $post2=post::with(['post_kategori_log'])->orderBy('id','desc')->paginate(5);
        return view('home',compact('post','post1','post2'));
    }
    public function blog(Request $request)
    {
        $cari=$request->get('q');
            //dd($cari);
            if ($cari!=null) {
                $post=post::with(['post_kategori_log'])
                ->where('judul', 'like', '%' . $cari . '%')
                ->orWhere('slug', 'like', '%' . $cari . '%')
                ->orWhere('deskripsi', 'like', '%' . $cari . '%')
                ->orWhere('tag', 'like', '%' . $cari . '%')
                            ->orderBy('id','DESC')
                            ->paginate(6);
            }else{
                $post=post::with(['post_kategori_log'])->orderBy('id','desc')->paginate(6);
            }
        return view('blog',compact('post'));
    }
    public function populer()
    {
        $post1=post::with(['post_kategori_log'])->paginate(4);
        dd($post1->toArray());
        return view('populer',compact('post1'));
    }
    public function kategori($slug)
    {
        $post=post_kategori::with(['post_kategori_log'])
                                ->where('slug','=',$slug)
                                ->first();
        if ($post!=null&&$post->post_kategori_log->toArray()!=[]) {
            $post=post_kategori::with(['post_kategori_log'=>function ($query)
                                    {
                                        return $query->with('post')->get();
                                    }])
                                ->where('slug','=',$slug)
                                ->get();
        return view('kategori',compact('post'));
        }else{
            return view('404');
        }
        
    }
    public function tag($slug)
    {
        $kategori=post_kategori::all();
        $posting=post::all();
        $slug=str_replace('-',' ',$slug);
        $title=ucfirst(trans($slug));
        $post=post::where('tag','like',"%{$slug}%")->first();
        $post1=post::with(['post_kategori_log'])->inRandomOrder()->paginate(5);
            $tag = $post->tag;
            $tag = str_replace(array("\n", "\r"), '', $tag);;
            $arr_tag = explode (",",$tag);
            //$why=post::where('tag','=',$arr_tag)->get();
        return view('post_tag',compact('post','kategori','post1','posting','arr_tag'));
    }
    public function post($slug)
    {
        $kategori=post_kategori::all();
        $postingan=post::with(['user','post_kategori_log'])->where('slug','=',$slug)->first();
        $posting=post::all();
        $post=post::with(['post_kategori_log'])->where('slug','=',$slug)->count();
        if ($post!=0) {
            $post=post::with(['user','post_kategori_log'])->where('slug','=',$slug)->first();
            $comment=post_comment::where('post_id','=',$postingan->id)->get();
            $post1=post::with(['user','post_kategori_log'])->inRandomOrder()->paginate(5);
            $tag = $post->tag;
            $tag = str_replace(array("\n", "\r"), '', $tag);;
            $arr_tag = explode (",",$tag);
            //dd($post->toArray());
            return view('post',compact('post','kategori','post1','posting','arr_tag','comment'));
        }else{
            return view('404');
        }
    }
    public function notfound()
    {
        return view('404');
    }
    public function sitemap()
    {
        $html='<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xhtml="http://www.w3.org/1999/xhtml">';
        $posts = post::select('slug','created_at','updated_at')->get();
        return response()->view('sitemap', [
            'posts' => $posts
        ])->header('Content-Type', 'text/xml');
        // dd($data->toArray());
    }
    public function login()
    {
        return view('login');
    }
    public function postmap()
        {
            $html='<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xhtml="http://www.w3.org/1999/xhtml">';
            $posts = post::select('slug','created_at','updated_at')->get();
            return response()->view('sitemap', [
                'posts' => $posts
            ])->header('Content-Type', 'text/xml');
            // dd($data->toArray());
    }
    public function comment(Request $request)
    {
    $request->validate([
        'post_id'=>['required'],
        'nama'=>['required'],
        'email'=>['required'],
        'comment'=>['required'],
      ]);
    post_comment::create([ 
        'post_id'=>$request->get('post_id'),
        'nama'=>$request->get('nama'),
        'email'=>$request->get('email'),
        'comment'=>$request->get('comment'),
    ]);
    return redirect()->back()->with('success', 'Berhasil Dibuat');
}
}
