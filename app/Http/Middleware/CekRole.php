<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class CekRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        $roles = $this->CekRoute($request->route());
        $req_cek=$request->user();
        //dd($req_cek);
        if ($req_cek!=null) {
            if( $request->user()->hasRole($roles) || !$roles)
                {
                    return $next($request);
                }
        }
       
        return redirect('/login')->with('gagal', 'Silahkan login kembali');
    }
    private function CekRoute($route)
    {
        $actions = $route->getAction();
        return isset($actions['roles']) ? $actions['roles'] : null;
    }
}
