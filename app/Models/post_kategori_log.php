<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class post_kategori_log extends Model
{
    use HasFactory;
    protected $fillable = [
        'post_id',
        'post_kategori_id',
    ];
    public function post()
    {
        return $this->belongsTo('App\Models\post','post_id');
    }
    public function post_kategori()
    {
        return $this->belongsTo('App\Models\post_kategori','post_kategori_id');
    }
}
