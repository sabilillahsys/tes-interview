<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class post extends Model
{
    use HasFactory;
    protected $fillable = [
        'judul',
        'slug',
        'deskripsi',
        'isi',
        'tag',
        'img',
        'alt',
        'status',
        'user_id',
    ];
    public function user()
    {
        return $this->belongsTo('App\Models\User','user_id');
    }
    public function post_kategori_log()
    {
        return $this->hasMany('App\Models\post_kategori_log');
    }
}
