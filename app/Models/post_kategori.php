<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class post_kategori extends Model
{
    use HasFactory;
    protected $fillable = [
        'judul',
        'slug',
        'deskripsi',
    ];
    public function post_kategori_log()//menampilkan user dengan data tes yang diikuti
    {
        return $this->hasMany('App\Models\post_kategori_log');
    }
}
