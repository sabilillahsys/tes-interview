<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class post_comment extends Model
{
    use HasFactory;
    protected $fillable = [
        'post_id',
        'nama',
        'email',
        'comment',
    ];
    public function post()
    {
        return $this->belongsTo('App\Models\post','post_id');
    }
}
