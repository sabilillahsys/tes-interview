<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        //1
        DB::table('roles')->insert([
            'name' => "Master",
            'description' => "Digunakan sebagai Role Admin Utama",
        ]);
        DB::table('roles')->insert([
            'name' => "Penulis",
            'description' => "Digunakan sebagai Role Penulis",
        ]);
        DB::table('users')->insert([
            'name'=>"Master", 
            'username'=>"master", 
            'password'=>'$2y$10$AXToTcu4tCI/EhnhHVF06e13fdEYf9JH5RcK4MJPOvPk/s3EuIjPq',
            'pic'=>"belum",
            'hp'=>"belum",
            'jk'=>"belum",
            'status'=>"belum",
            'role_id'=>1,
        ]);
    }

}
