<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('post_kategori_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('post_id')->unsigned();
            $table->foreign('post_id')
                  ->references('id')
                  ->on('posts')
                  ->onDelete('CASCADE')
                  ->onUpdate('cascade');
            $table->integer('post_kategori_id')->unsigned();
            $table->foreign('post_kategori_id')
                  ->references('id')
                  ->on('post_kategoris')
                  ->onDelete('CASCADE')
                  ->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('post_kategori_logs');
    }
};
